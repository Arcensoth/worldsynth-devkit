## A very initial development kit for WorldSynth  
This is made with the intention of being helpful in getting started working with WorldSynth code by providing utilities for setting up the workspace and compiling.  
It's very new and limited for now, and very few things are working, but the plan is to be able to use this for seting up workspace for working on WorldSynth core projects, or for anyone working on library Add-Ons.

## How to use  
WorldSynth is created with Java8 + JavaFX enviroments as target, so you will need a Java8 + JavaFX compatible JDK  
The Oracle Java8 JDK comes with JavaFX, but we recommend using an OpenJDK + OpenJFX build over OracleJDK.  

- If you are using a linux enviroment, you may be able to install OpenJDK8 and OpenJFX builds using the distributions package manager like described below. If not, we recommend or use an OpenJDK build with OpenJFX, like ZuluFX JDK.  
In ubuntu you may be able to install install OpenJDK8 + OpenJFX8 + OpenJFX8-source with the command:  
```sudo apt install openjdk-8-jdk openjfx=8u161-b12-1ubuntu2 libopenjfx-java=8u161-b12-1ubuntu2 openjfx-source=8u161-b12-1ubuntu2 libopenjfx-jni=8u161-b12-1ubuntu2```  
- If you are using a windows enviroment we recomend using an OpenJDK build with JavaFX, like ZuluFX JDK.  
- If you are using a mac enviroment we recomend using an OpenJDK build with JavaFX, like ZuluFX JDK.  
  
### Setup eclipse
- Clone this devkit repository  
- Run the command './gradlew submodulesUpdate'  
	This will download and update all the git submodules  
- Run the command './gradlew submodulesEclipse'  
	This will setup all the submodules except for the forge project for importing in eclipse  
- Start eclipse and import the projects in the devkit as existing projects 
- Fix buildpaths  
	- Because WorldSynth uses JavaFX you will need to change the buildpath JRE System Library for the projects to use you JDK install that includes JFX.  
	- worldsynth-patcher needs worldsynth-engine as project dependency in the buildpath.  
	- worldsynth-composer needs worldsynth-engine as project dependency in the buildpath.  
	- worldsynth-lib_minecraft needs worldsynth-engine, worldsynth-patcher andd opennbt as project dependencies in the buildpath.  
